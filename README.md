# H5P MathInput

Implements Visual Math Editor for H5P in Drupal versions 9 to 11.

You can find more information about math inputs in the
[official documentation](https://h5p.org/mathematical-expressions).

## INSTALLATION
- Add module to a Drupal site running supported version.
- Install module.
  - Please make sure that H5P MathDisplay library is present.

**NB! Any content types with math equations will need to be edited in order
for the library to be added and math expressions to be displayed properly.**

## EXTRA STEPS AFTER INSTALLATION
- Add H5P module MathDisplay (https://github.com/h5p/h5p-math-display)
- Please visit the
  [official documentation](https://h5p.org/mathematical-expressions) page for
  packaged version of the library.
